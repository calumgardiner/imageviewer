package co.xcvb.imageviewer.model;

import java.io.File;
import java.security.InvalidParameterException;

import co.xcvb.imageviewer.model.filter.ImageFilter;

/**
 * <p>
 * ImageDirectory, represents a directory, selecting only the image files.
 * </p>
 * 
 * @author Calum Gardiner
 *
 */
public class ImageDirectory {

	private File directory;

	/**
	 * Construct an ImageDirectory with a string to the location.
	 * 
	 * @param directory
	 */
	public ImageDirectory(String directory) {
		this(new File(directory));
	}

	/**
	 * Construct an ImageDirectory with a File object.
	 * 
	 * @param directory
	 */
	public ImageDirectory(File directory) {
		this.directory = directory;
		if (!this.directory.isDirectory())
			throw new InvalidParameterException("The specified directory is not a directory...");
	}

	/**
	 * Get a list of the image files in this directory.
	 * 
	 * @return list of image files
	 */
	public File[] getImageFiles() {
		return this.directory.listFiles(new ImageFilter());
	}

	public File getFile() {
		return this.directory;
	}

}