package co.xcvb.imageviewer.model;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 * <p>
 * Proportional image, can be resized and keep its proportion. Composed of
 * BufferedImage, but can return different implementations of java images.
 * </p>
 * 
 * @author Calum Gardiner
 * @see BufferedImage
 * @see Image
 * @see ImageIcon
 *
 */
public class PImage {

	private BufferedImage image;
	private String absoluteFile;
	private int oWidth;
	private int oHeight;
	private int width;
	private int height;
	private int HINTS = Image.SCALE_SMOOTH;

	/**
	 * Construct a PImage on the path/to/image
	 * 
	 * @param image
	 */
	public PImage(String image) {
		this(new File(image));
	}

	/**
	 * Construct a PImage on a File object which refers to the image.
	 * 
	 * @param image
	 */
	public PImage(File image) {
		try {
			this.image = ImageIO.read(image);
			this.absoluteFile = image.getAbsolutePath();
			this.oWidth = this.image.getWidth();
			this.width = this.image.getWidth();
			this.oHeight = this.image.getHeight();
			this.height = this.image.getHeight();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get the original width of the image.
	 * 
	 * @return original width
	 */
	public int getOriginalWidth() {
		return this.oWidth;
	}

	/**
	 * Get the original height of the image.
	 * 
	 * @return original height
	 */
	public int getOriginalHeight() {
		return this.oHeight;
	}

	/**
	 * Get the current width of the image.
	 * 
	 * @return current width
	 */
	public int getCurrentWidth() {
		return this.width;
	}

	/**
	 * Get the current height of the image.
	 * 
	 * @return current height
	 */
	public int getCurrentHeight() {
		return this.height;
	}

	/**
	 * Get the absolute path of the image file.
	 * 
	 * @return image file path
	 */
	public String getFilePath() {
		return this.absoluteFile;
	}

	/**
	 * Resizes the image proportionally to the given new width.
	 * 
	 * @param width
	 */
	public void resizeToWidth(int width) {
		this.width = width;
		this.height = (int) ((double) width * (double) oHeight / (double) oWidth);
		resizeImageHeld();
	}

	/**
	 * Resizes the image proportionally to the given new height.
	 * 
	 * @param height
	 */
	public void resizeToHeight(int height) {
		this.height = height;
		this.width = (int) ((double) height * (double) oWidth / (double) oHeight);
		resizeImageHeld();
	}

	/**
	 * Loads a new instance of the image and resizes to the correct current
	 * width and height.
	 */
	private void resizeImageHeld() {
		BufferedImage image = null;
		try {
			image = ImageIO.read(new File(absoluteFile));
		} catch (IOException e) {
			e.printStackTrace();
		}
		image.getScaledInstance(width, height, HINTS);
		this.image = image;
	}

	/**
	 * Set the hints used when resizing.
	 * 
	 * @see Image
	 * @param hints
	 */
	public void setImageHints(int hints) {
		this.HINTS = hints;
	}

	/**
	 * Get the resized BufferedImage.
	 * 
	 * @return buffered image
	 * @see BufferedImage
	 */
	public BufferedImage getBufferedImage() {
		return this.image;
	}

	/**
	 * Get an ImageIcon of the resized image. This format will retain animation
	 * such as .gif images. This method will create a new ImageIcon object and
	 * return it.
	 * 
	 * @return image icon
	 * @see ImageIcon
	 */
	public ImageIcon getImageIcon() {
		ImageIcon icon = new ImageIcon(absoluteFile);
		int hints = HINTS;
		if (absoluteFile.toLowerCase().endsWith("gif")) {
			hints = Image.SCALE_FAST;
		}
		icon.setImage(icon.getImage().getScaledInstance(width, height, hints));
		return icon;
	}
}
