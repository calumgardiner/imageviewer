package co.xcvb.imageviewer.model.filter;

import java.io.File;
import java.io.FilenameFilter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 * Implementation of FilenameFilter which only accepts image extensions.
 * </p>
 * 
 * @author Calum Gardiner
 * @see FilenameFilter
 *
 */
public class ImageFilter implements FilenameFilter {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
	 */
	@Override
	public boolean accept(File dir, String name) {
		String imgExtRegex = ".*\\.(gif|jpg|jpeg|png)";
		Pattern pattern = Pattern.compile(imgExtRegex);
		Matcher matcher = pattern.matcher(name.toLowerCase());
		return matcher.matches();
	}

}