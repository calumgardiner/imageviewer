package co.xcvb.imageviewer.resources.image;

import javax.swing.ImageIcon;

/**
 * <p>
 * Image class loader for this project.
 * </p>
 * 
 * @author Calum Gardiner
 *
 */
public enum ImageResource {

	LOADING("loading.gif");

	private ImageIcon image;
	private static final String IMAGE_LOCATION = "co/xcvb/imageviewer/resources/image/images/";

	ImageResource(String image) {
		this.image = new ImageIcon(this.getClass().getClassLoader().getResource(IMAGE_LOCATION + image));
	}

	public ImageIcon getImage() {
		return this.image;
	}
}
