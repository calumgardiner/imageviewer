package co.xcvb.imageviewer.ui.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import co.xcvb.imageviewer.model.ImageDirectory;
import co.xcvb.imageviewer.resources.image.ImageResource;
import co.xcvb.imageviewer.ui.components.thread.ThumbnailLoader;

/**
 * <p>
 * Creates a scrollable gallery of images based on an ImageDirectory.
 * </p>
 * 
 * @author Calum Gardiner
 *
 */
public class GalleryPanel extends ObservableJPanel {

	private static final long serialVersionUID = 3988163968852756002L;
	private final static ImageIcon LOADING = ImageResource.LOADING.getImage();
	private ImageDirectory directory;
	private List<ImagePanel> thumbnails;
	private int height;
	private JPanel scrollPane;
	private ThumbnailLoader runnableLoader;

	public GalleryPanel(int height) {
		super();
		this.height = height;
		this.scrollPane = new JPanel(new FlowLayout());
		this.setLayout(new BorderLayout());
		this.add(new JScrollPane(scrollPane), BorderLayout.CENTER);
		this.setHeight(height);
	}

	public GalleryPanel(ImageDirectory directory, int height) {
		this(height);
		this.directory = directory;
		refreshPanel();
	}

	public void setDirectory(ImageDirectory directory) {
		this.directory = directory;
		refreshPanel();
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public void refreshPanel() {
		if (directory != null) {
			cleanUp();
			for (File file : directory.getImageFiles()) {
				ImagePanel initialPanel = new ImagePanel(LOADING);
				initialPanel.addMouseListener(new MouseAdapter() {
					public void mouseClicked(MouseEvent e) {
						notifyObservers(file);
					}
				});
				initialPanel.setToolTipText(file.getName());
				initialPanel.setCursor(new Cursor(Cursor.HAND_CURSOR));
				initialPanel.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 2));
				this.thumbnails.add(initialPanel);
			}
			for (ImagePanel thumbnail : thumbnails) {
				this.scrollPane.add(thumbnail);
			}
			this.runnableLoader = new ThumbnailLoader(directory.getImageFiles(), thumbnails, height);
			Thread thumbnailLoader = new Thread(runnableLoader);
			thumbnailLoader.start();
		}
	}

	private void cleanUp() {
		if (this.runnableLoader != null) {
			this.runnableLoader.stop();
		}
		if (thumbnails != null && thumbnails.size() > 0) {
			thumbnails.clear();
		} else {
			this.thumbnails = new ArrayList<ImagePanel>();
		}
		this.scrollPane.removeAll();
		this.scrollPane.repaint();
	}
}
