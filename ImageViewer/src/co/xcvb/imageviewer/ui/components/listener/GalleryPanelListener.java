package co.xcvb.imageviewer.ui.components.listener;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import co.xcvb.imageviewer.ui.components.GalleryPanel;

public class GalleryPanelListener extends ComponentAdapter {

	private GalleryPanel panel;

	public GalleryPanelListener(GalleryPanel panel) {
		this.panel = panel;
	}

	@Override
	public void componentResized(ComponentEvent e) {
		this.panel.setHeight(panel.getHeight());
		this.panel.refreshPanel();
	}
}
