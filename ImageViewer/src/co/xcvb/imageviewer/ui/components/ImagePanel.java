package co.xcvb.imageviewer.ui.components;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * <p>
 * ImagePanel, provides an extended JPanel with centered JLabel holding an
 * ImageIcon.
 * </p>
 * 
 * @author Calum Gardiner
 * @see JPanel
 * @see ImageIcon
 *
 */
public class ImagePanel extends JPanel {

	private static final long serialVersionUID = 121681752485082530L;
	private ImageIcon image;
	private JLabel imageLabel;

	/**
	 * Construct ImagePanel with no image currently set.
	 */
	public ImagePanel() {
		super();
		initializeUI();
	}

	/**
	 * Construct an ImagePanel with the image pre set.
	 * 
	 * @param image
	 */
	public ImagePanel(ImageIcon image) {
		super();
		initializeUI();
		this.image = image;
		setImageAndRepaint();
	}

	public void setImage(ImageIcon image) {
		this.image = image;
		setImageAndRepaint();
	}

	private void initializeUI() {
		this.setLayout(new BorderLayout());
		this.imageLabel = new JLabel();
		this.add(imageLabel, BorderLayout.CENTER);
	}

	private void setImageAndRepaint() {
		this.imageLabel.setIcon(image);
		this.imageLabel.repaint();
		this.repaint();
	}

}
