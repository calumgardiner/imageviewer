package co.xcvb.imageviewer.ui.components;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import co.xcvb.imageviewer.model.ImageDirectory;
import co.xcvb.imageviewer.model.PImage;
import co.xcvb.imageviewer.resources.image.ImageResource;

public class MainFrame extends JFrame implements Observer {

	private static final long serialVersionUID = -7433988071901745542L;

	public static void main(String... args) {
		new MainFrame();
	}

	private ImagePanel currentImage;
	private GalleryPanel galleryPanel;
	private static final ImageIcon LOADING = ImageResource.LOADING.getImage();

	public MainFrame() {
		super("XD Image Viewer");
		setSystemLookAndFeel();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new BorderLayout());
		this.currentImage = new ImagePanel();
		this.galleryPanel = new GalleryPanel(100);
		this.galleryPanel.setPreferredSize(new Dimension(800, 150));
		this.getContentPane().add(galleryPanel, BorderLayout.PAGE_START);
		this.getContentPane().add(currentImage, BorderLayout.CENTER);
		this.setSize(1000, 800);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(screenSize.width / 2 - 500, screenSize.height / 2 - 400);
		setupMenuBar();
		this.setVisible(true);
		this.galleryPanel.addObserver(this);
	}

	private void setSystemLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
	}

	private void setupMenuBar() {
		MenuBar menuBar = new MenuBar();
		Menu file = new Menu("File");
		MenuItem open = new MenuItem("Open");
		file.add(open);
		open.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				chooseDir();
			}
		});
		MenuItem close = new MenuItem("Close");
		file.add(close);
		close.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		menuBar.add(file);
		this.setMenuBar(menuBar);
	}

	@Override
	public void update(Observable o, Object arg) {
		if (arg instanceof File) {
			this.currentImage.setImage(LOADING);
			PImage mainImage = new PImage((File) arg);
			if (mainImage.getOriginalHeight() > mainImage.getOriginalWidth()) {
				mainImage.resizeToHeight(currentImage.getHeight());
			} else {
				mainImage.resizeToWidth(currentImage.getWidth());
			}
			this.currentImage.setImage(mainImage.getImageIcon());
		}
	}

	private void chooseDir() {
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setDialogType(JFileChooser.OPEN_DIALOG);
		chooser.setDialogTitle("Select directory");
		int returnVal = chooser.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			this.galleryPanel.setDirectory(new ImageDirectory(chooser.getSelectedFile()));
		}
	}
}
