package co.xcvb.imageviewer.ui.components;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

public class ObservableJPanel extends JPanel {

	private static final long serialVersionUID = -345237196511779508L;
	private Observable observable;

	public ObservableJPanel() {
		super();
		this.observable = new Observable() {
			@Override
			public void notifyObservers(Object arg) {
				this.setChanged();
				super.notifyObservers(arg);
			}
		};
	}

	public void addObserver(Observer o) {
		this.observable.addObserver(o);
	}

	public void removeObserver(Observer o) {
		this.observable.addObserver(o);
	}

	public void deleteObservers() {
		this.observable.deleteObservers();
	}

	public void notifyObservers(Object arg) {
		this.observable.notifyObservers(arg);
	}
}
