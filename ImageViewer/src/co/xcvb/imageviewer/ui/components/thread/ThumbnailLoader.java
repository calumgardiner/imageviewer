package co.xcvb.imageviewer.ui.components.thread;

import java.io.File;
import java.util.List;

import co.xcvb.imageviewer.model.PImage;
import co.xcvb.imageviewer.ui.components.ImagePanel;

public class ThumbnailLoader implements Runnable {

	private File[] imageFiles;
	private List<ImagePanel> loadingPanels;
	private int height;
	private boolean running;

	public ThumbnailLoader(File[] imageFiles, List<ImagePanel> loadingPanels, int height) {
		this.imageFiles = imageFiles;
		this.loadingPanels = loadingPanels;
		this.height = height;
		this.running = true;
	}

	@Override
	public void run() {
		while (running) {
			for (int i = 0; i < imageFiles.length; i++) {
				PImage thumbnailImage = new PImage(imageFiles[i]);
				thumbnailImage.resizeToHeight(height);
				loadingPanels.get(i).setImage(thumbnailImage.getImageIcon());
			}
		}
	}

	public void stop() {
		this.running = false;
	}

}